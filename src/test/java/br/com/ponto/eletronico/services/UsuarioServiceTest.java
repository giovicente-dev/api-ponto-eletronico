package br.com.ponto.eletronico.services;

import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setNome("Giovanni");
        usuario.setCpf("560.608.910-63");
        usuario.setEmail("lbsullivan9@vingcesscar.tk");
        usuario.setDataCadastro(LocalDate.now());
    }

    @Test
    public void testarCriarUsuario() {
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
        Usuario usuarioTest = usuarioService.criarUsuario(usuario);

        Assertions.assertSame(usuario, usuarioTest);
    }

    @Test
    public void testarConsultarUsuarios() {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuarioIterable = usuarioService.consultarUsuarios();

        Assertions.assertEquals(usuarios, usuarioIterable);
    }

    @Test
    public void testarConsultarUsuarioPorIdExistente() {
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(usuario));

        Usuario usuarioTest = usuarioService.consultarUsuarioPorId(usuario.getId());

        Assertions.assertSame(usuario, usuarioTest);
    }

    @Test
    public void testarConsultarUsuarioPorIdInexistente() {
        Mockito.when(usuarioRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.consultarUsuarioPorId(Mockito.anyLong());});
    }

    @Test
    public void testarAtualizarUsuarioPorIdExistente() {
        Usuario usuarioTeste = new Usuario();
        usuarioTeste.setNome("Giovanni");
        usuarioTeste.setCpf("113.770.640-69");
        usuarioTeste.setEmail("muha@acunsleep.tk");

        Mockito.when(usuarioRepository.existsById(Mockito.anyLong())).thenReturn(true);
        Mockito.when(usuarioService.criarUsuario(usuarioTeste)).thenReturn(usuarioTeste);

        Assertions.assertEquals(usuario.getId(), usuarioTeste.getId());
    }

    @Test
    public void testarAtualizarUsuarioPorIdInexistente() {
        Mockito.when(usuarioRepository.existsById(Mockito.anyLong())).thenReturn(false);

        Assertions.assertThrows(RuntimeException.class, () -> {usuarioService.atualizarUsuario(Mockito.anyLong(), usuario);});
    }

}
