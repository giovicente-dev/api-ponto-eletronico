package br.com.ponto.eletronico.services;

import br.com.ponto.eletronico.enums.TipoRegistroEnum;
import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOSaidaConsulta;
import br.com.ponto.eletronico.models.RegistroPonto;
import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.repositories.RegistroPontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootTest
public class RegistroPontoServiceTest {

    @MockBean
    private RegistroPontoRepository registroPontoRepository;

    @Autowired
    private RegistroPontoService registroPontoService;

    Usuario usuario;
    RegistroPonto registroPonto;
    RegistroPontoDTOSaidaConsulta registroPontoDTOSaidaConsulta;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Giovanni");
        usuario.setCpf("560.608.910-63");
        usuario.setEmail("lbsullivan9@vingcesscar.tk");
        usuario.setDataCadastro(LocalDate.now());

        registroPonto = new RegistroPonto();
        registroPonto.setId(1);
        registroPonto.setUsuario(usuario);
        registroPonto.setTipoRegistro(TipoRegistroEnum.ENTRADA);
        registroPonto.setDataHoraRegistro(LocalDateTime.now());

    }


    @Test
    public void testarRegistrarPonto() {
        Mockito.when(registroPontoRepository.save(registroPonto)).thenReturn(registroPonto);
        RegistroPonto registroPontoTest = registroPontoService.registrarPonto(registroPonto);

        Assertions.assertSame(registroPonto, registroPontoTest);
    }

    @Test
    public void testarConsultarPonto() {
        Iterable<RegistroPonto> registros = Arrays.asList(registroPonto);

        registroPontoDTOSaidaConsulta = new RegistroPontoDTOSaidaConsulta();
        registroPontoDTOSaidaConsulta.setRegistrosPonto(registros);

        Mockito.when(registroPontoRepository.findAllByUsuarioId(Mockito.anyLong())).thenReturn(registros);

        RegistroPontoDTOSaidaConsulta registroPontoDTOSaidaTest = registroPontoService.consultarPonto(usuario.getId());

        Assertions.assertEquals(registros, registroPontoDTOSaidaTest.getRegistrosPonto());
        Assertions.assertEquals(registroPontoDTOSaidaConsulta.getHorasTrabalhadas(), registroPontoDTOSaidaTest.getHorasTrabalhadas());
    }
}
