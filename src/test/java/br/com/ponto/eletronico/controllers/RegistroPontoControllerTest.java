package br.com.ponto.eletronico.controllers;

import br.com.ponto.eletronico.enums.TipoRegistroEnum;
import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOEntradaInserir;
import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOSaidaConsulta;
import br.com.ponto.eletronico.models.RegistroPonto;
import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.services.RegistroPontoService;
import br.com.ponto.eletronico.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

@WebMvcTest(RegistroPontoController.class)
public class RegistroPontoControllerTest {

    @MockBean
    private RegistroPontoService registroPontoService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    RegistroPonto registroPonto;
    RegistroPontoDTOEntradaInserir registroPontoDTOEntradaInserir;
    RegistroPontoDTOSaidaConsulta registroPontoDTOSaidaConsulta;

    @Test
    public void testarPostRegistroPonto() throws Exception {

        usuario = new Usuario();
        usuario.setId(1);

        registroPontoDTOEntradaInserir = new RegistroPontoDTOEntradaInserir();
        registroPontoDTOEntradaInserir.setUsuario(usuario);
        registroPontoDTOEntradaInserir.setTipoRegistro(TipoRegistroEnum.ENTRADA);

        registroPonto = registroPontoDTOEntradaInserir.converterRegistroPonto();

        Mockito.when(registroPontoService.registrarPonto(Mockito.any(RegistroPonto.class))).then(registroPontoObjeto -> {
            registroPonto.setId(1);
            return registroPonto;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonRegistroPonto = mapper.writeValueAsString(registroPontoDTOEntradaInserir);

        mockMvc.perform(MockMvcRequestBuilders.post("/registrosdeponto")
               .contentType(MediaType.APPLICATION_JSON)
               .content(jsonRegistroPonto))
               .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarGetRegistros() throws Exception {

        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Giovanni");
        usuario.setCpf("560.608.910-63");
        usuario.setEmail("lbsullivan9@vingcesscar.tk");
        usuario.setDataCadastro(LocalDate.now());

        registroPonto = new RegistroPonto();
        registroPonto.setId(1);
        registroPonto.setTipoRegistro(TipoRegistroEnum.ENTRADA);
        registroPonto.setDataHoraRegistro(LocalDateTime.now());
        registroPonto.setUsuario(usuario);

        ArrayList<RegistroPonto> registros = new ArrayList<>();
        registros.add(registroPonto);

        Iterable<RegistroPonto> registroPontoIterable = registros;

        registroPontoDTOSaidaConsulta = new RegistroPontoDTOSaidaConsulta();
        registroPontoDTOSaidaConsulta.setRegistrosPonto(registros);

        Mockito.when(registroPontoService.consultarPonto(Mockito.anyLong())).thenReturn(registroPontoDTOSaidaConsulta);

        ObjectMapper mapper = new ObjectMapper();
        String jsonRegistroPontoIterable = mapper.writeValueAsString(registroPontoDTOSaidaConsulta);

        mockMvc.perform(MockMvcRequestBuilders.get("/registrosdeponto/1")
               .contentType(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
