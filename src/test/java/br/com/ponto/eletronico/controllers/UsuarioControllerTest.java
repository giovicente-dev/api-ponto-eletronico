package br.com.ponto.eletronico.controllers;

import br.com.ponto.eletronico.models.DTOs.UsuarioDTOEntradaAtualizar;
import br.com.ponto.eletronico.models.DTOs.UsuarioDTOEntradaInserir;
import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    UsuarioDTOEntradaInserir usuarioDTOEntradaInserir;
    UsuarioDTOEntradaAtualizar usuarioDTOEntradaAtualizar;

    @Test
    public void testarPostUsuarioValido() throws Exception {

      usuarioDTOEntradaInserir = new UsuarioDTOEntradaInserir();
      usuarioDTOEntradaInserir.setNome("Giovanni");
      usuarioDTOEntradaInserir.setCpf("560.608.910-63");
      usuarioDTOEntradaInserir.setEmail("lbsullivan9@vingcesscar.tk");

      usuario = usuarioDTOEntradaInserir.converterParaUsuario();

      Mockito.when(usuarioService.criarUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
          usuario.setId(1);
          return usuario;
      });

      ObjectMapper mapper = new ObjectMapper();
      String jsonUsuario = mapper.writeValueAsString(usuarioDTOEntradaInserir);

      mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
             .contentType(MediaType.APPLICATION_JSON)
             .content(jsonUsuario))
             .andExpect(MockMvcResultMatchers.status().isCreated())
             .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarGetUsuarios() throws Exception {

       usuario = new Usuario();
       usuario.setId(1);
       usuario.setNome("Giovanni");
       usuario.setCpf("560.608.910-63");
       usuario.setEmail("lbsullivan9@vingcesscar.tk");
       usuario.setDataCadastro(LocalDate.now());

       ArrayList<Usuario> usuarios = new ArrayList<>();
       usuarios.add(usuario);

       Iterable<Usuario> usuarioIterable = usuarios;

       Mockito.when(usuarioService.consultarUsuarios()).thenReturn(usuarioIterable);

       ObjectMapper mapper = new ObjectMapper();
       String jsonUsuarioIterable = mapper.writeValueAsString(usuarioIterable);

       mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
              .contentType(MediaType.APPLICATION_JSON))
              .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarGetUsuariosPorID() throws Exception {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setNome("Giovanni");
        usuario.setCpf("560.608.910-63");
        usuario.setEmail("lbsullivan9@vingcesscar.tk");
        usuario.setDataCadastro(LocalDate.now());

        Mockito.when(usuarioService.consultarUsuarioPorId(Mockito.anyLong())).thenReturn(usuario);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
               .pathInfo("/{id}"))
               .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarGetUsuariosPorIDInexistente() throws Exception {
        Mockito.when(usuarioService.consultarUsuarioPorId(Mockito.anyLong())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
               .pathInfo("/{id}"))
               .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarPutAtualizarUsuarios() throws Exception {
        usuario = new Usuario();
        usuario.setNome("Giovanni");
        usuario.setCpf("560.608.910-63");
        usuario.setEmail("lbsullivan9@vingcesscar.tk");
        usuario.setDataCadastro(LocalDate.now());

        usuarioDTOEntradaAtualizar = new UsuarioDTOEntradaAtualizar();
        usuarioDTOEntradaAtualizar.setNome("Giovanni Vicente");
        usuario = usuarioDTOEntradaAtualizar.converterParaUsuario();

        Mockito.when(usuarioService.atualizarUsuario(Mockito.anyLong(), usuario)).then(usuarioObjeto -> {
            usuario.setId(1);
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuarioAtualizado = mapper.writeValueAsString(usuarioDTOEntradaAtualizar);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/{id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonUsuarioAtualizado))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}


