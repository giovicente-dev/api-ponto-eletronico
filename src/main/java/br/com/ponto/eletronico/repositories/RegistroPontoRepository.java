package br.com.ponto.eletronico.repositories;

import br.com.ponto.eletronico.models.RegistroPonto;
import org.springframework.data.repository.CrudRepository;

public interface RegistroPontoRepository extends CrudRepository<RegistroPonto, Long> {
    Iterable<RegistroPonto> findAllByUsuarioId(long id);
}
