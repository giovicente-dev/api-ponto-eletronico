package br.com.ponto.eletronico.repositories;

import br.com.ponto.eletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> { }
