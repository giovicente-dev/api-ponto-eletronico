package br.com.ponto.eletronico.services;

import br.com.ponto.eletronico.enums.TipoRegistroEnum;
import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOSaidaConsulta;
import br.com.ponto.eletronico.models.RegistroPonto;
import br.com.ponto.eletronico.repositories.RegistroPontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class RegistroPontoService {

    @Autowired
    private RegistroPontoRepository registroPontoRepository;

    public RegistroPonto registrarPonto(RegistroPonto registroPonto) {
        LocalDateTime dataHora = LocalDateTime.now();
        registroPonto.setDataHoraRegistro(dataHora);

        RegistroPonto registroPontoObjeto = registroPontoRepository.save(registroPonto);
        return registroPontoObjeto;
    }

    public RegistroPontoDTOSaidaConsulta consultarPonto(long idUsuario) {

        // Obtém registros de ponto por id de usuário e coloca em um Iterable
        RegistroPontoDTOSaidaConsulta registroPontoDTOSaidaConsulta = new RegistroPontoDTOSaidaConsulta();
        registroPontoDTOSaidaConsulta.setRegistrosPonto(registroPontoRepository.findAllByUsuarioId(idUsuario));

        // Coloca os registros de ponto do Iterable em um ArrayList para manipulação das informações
        ArrayList<RegistroPonto> jornada = new ArrayList<>();
        registroPontoDTOSaidaConsulta.getRegistrosPonto().forEach(jornada::add);

        // Cria um registro de entradas e saídas para futura realização do cálculo
        ArrayList<LocalDateTime> entradas = new ArrayList<>();
        ArrayList<LocalDateTime> saidas = new ArrayList<>();

        // A partir do ArrayList de jornada, popula os Arrays de entradas e saídas
        for (int i = 0; i < jornada.size(); i++) {
            if (jornada.get(i).getTipoRegistro() == TipoRegistroEnum.ENTRADA) {
                entradas.add(jornada.get(i).getDataHoraRegistro());
            } else {
                saidas.add(jornada.get(i).getDataHoraRegistro());
            }
        }

        // Cria as variáveis para cálculo das horas trabalhadas
        int saldoIntervalo = 0;
        int saldo = 0;

        // Realiza o cálculo das horas trabalhadas
        for (int j = 0; j < entradas.size(); j++) {
            for (int k = 0; k < saidas.size(); k++) {
                saldoIntervalo = saidas.get(j).getHour() - entradas.get(k).getHour();
                saldo += saldoIntervalo;
            }
        }

        // Após o cálculo do saldo, atribui este valor para o objeto a ser retornado
        registroPontoDTOSaidaConsulta.setHorasTrabalhadas(saldo);

        // Retorna o objeto
        return registroPontoDTOSaidaConsulta;
    }
}
