package br.com.ponto.eletronico.services;

import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario criarUsuario(Usuario usuario) {
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Iterable<Usuario> consultarUsuarios() {
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }

    public Usuario consultarUsuarioPorId(long id) {
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);

        if (optionalUsuario.isPresent()) {
            return optionalUsuario.get();
        }

        throw new RuntimeException("Usuário não encontrado");
    }

    public Usuario atualizarUsuario(long id, Usuario usuario) {
        if (usuarioRepository.existsById(id)) {
            usuario.setId(id);
            Usuario usuarioObjeto = criarUsuario(usuario);

            return usuarioObjeto;
        }

        throw new RuntimeException("Usuário não encontrado");
    }
}
