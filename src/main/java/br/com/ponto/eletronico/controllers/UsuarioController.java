package br.com.ponto.eletronico.controllers;

import br.com.ponto.eletronico.models.DTOs.UsuarioDTOEntradaAtualizar;
import br.com.ponto.eletronico.models.DTOs.UsuarioDTOEntradaInserir;
import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public ResponseEntity<Usuario> postUsuario(@RequestBody @Valid UsuarioDTOEntradaInserir usuarioDTOEntradaInserir) {
        Usuario usuario = usuarioDTOEntradaInserir.converterParaUsuario();
        Usuario usuarioObjeto = usuarioService.criarUsuario(usuario);

        return ResponseEntity.status(201).body(usuarioObjeto);
    }

    @GetMapping
    public Iterable<Usuario> getUsuarios() {
        Iterable<Usuario> usuarios = usuarioService.consultarUsuarios();
        return usuarios;
    }

    @GetMapping("/{id}")
    public Usuario getUsuariosPorID(@PathVariable(name = "id") long id) {
        try {
            Usuario usuario = usuarioService.consultarUsuarioPorId(id);
            return usuario;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Usuario> putAtualizarUsuarios(@PathVariable(name = "id") long id, @RequestBody @Valid UsuarioDTOEntradaAtualizar usuarioDTOEntradaAtualizar) {
        try {
            Usuario usuarioObjeto = usuarioService.consultarUsuarioPorId(id);
            Usuario usuario = usuarioDTOEntradaAtualizar.converterParaUsuario();
            usuario.setDataCadastro(usuarioObjeto.getDataCadastro());

            Usuario usuarioRetorno = usuarioService.atualizarUsuario(id, usuario);
            return ResponseEntity.status(204).build();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
