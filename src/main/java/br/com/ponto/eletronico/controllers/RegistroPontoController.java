package br.com.ponto.eletronico.controllers;

import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOEntradaInserir;
import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOSaidaConsulta;
import br.com.ponto.eletronico.models.DTOs.RegistroPontoDTOSaidaInserir;
import br.com.ponto.eletronico.models.RegistroPonto;
import br.com.ponto.eletronico.models.Usuario;
import br.com.ponto.eletronico.services.RegistroPontoService;
import br.com.ponto.eletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/registrosdeponto")
public class RegistroPontoController {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    RegistroPontoService registroPontoService;

    @PostMapping
    public ResponseEntity<RegistroPontoDTOSaidaInserir> postRegistroPonto(@RequestBody RegistroPontoDTOEntradaInserir registroPontoDTOEntradaInserir) {
        // Obtém dados do Usuário a partir do id informado
        long idUsuario = registroPontoDTOEntradaInserir.getUsuario().getId();
        Usuario usuario = usuarioService.consultarUsuarioPorId(idUsuario);

        // Cria um objeto de Registro de Ponto e associa o usuário obtido ao mesmo
        RegistroPonto registroPonto = registroPontoDTOEntradaInserir.converterRegistroPonto();
        registroPonto.setUsuario(usuario);

        // Chama a Service para salvar o Registro no banco de dados e guarda o retorno no registroPontoObjeto
        RegistroPonto registroPontoObjeto = registroPontoService.registrarPonto(registroPonto);

        // Cria um objeto da DTO de saída para retornar somente a data/hora e tipo do registro
        RegistroPontoDTOSaidaInserir registroPontoDTOSaidaInserir = new RegistroPontoDTOSaidaInserir();
        registroPontoDTOSaidaInserir.setDataHoraRegistro(registroPontoObjeto.getDataHoraRegistro());
        registroPontoDTOSaidaInserir.setTipoRegistro(registroPontoObjeto.getTipoRegistro());

        // Retorno de registro criado com body da DTO de saída
        return ResponseEntity.status(201).body(registroPontoDTOSaidaInserir);
    }

    @GetMapping("/{id_usuario}")
    public RegistroPontoDTOSaidaConsulta getRegistros(@PathVariable(name = "id_usuario") long id) {
        RegistroPontoDTOSaidaConsulta registroPontoDTOSaidaConsultaObjeto = registroPontoService.consultarPonto(id);
        return registroPontoDTOSaidaConsultaObjeto;
    }
}
