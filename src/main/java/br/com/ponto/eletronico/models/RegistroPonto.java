package br.com.ponto.eletronico.models;

import br.com.ponto.eletronico.enums.TipoRegistroEnum;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class RegistroPonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private Usuario usuario;

    private LocalDateTime dataHoraRegistro;
    private TipoRegistroEnum tipoRegistro;

    public RegistroPonto() { }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public Usuario getUsuario() { return usuario; }

    public void setUsuario(Usuario usuario) { this.usuario = usuario; }

    public LocalDateTime getDataHoraRegistro() { return dataHoraRegistro; }

    public void setDataHoraRegistro(LocalDateTime dataHoraRegistro) { this.dataHoraRegistro = dataHoraRegistro; }

    public TipoRegistroEnum getTipoRegistro() { return tipoRegistro; }

    public void setTipoRegistro(TipoRegistroEnum tipoRegistro) { this.tipoRegistro = tipoRegistro; }

}
