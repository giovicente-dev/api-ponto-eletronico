package br.com.ponto.eletronico.models.DTOs;

import br.com.ponto.eletronico.enums.TipoRegistroEnum;
import br.com.ponto.eletronico.models.RegistroPonto;
import br.com.ponto.eletronico.models.Usuario;

public class RegistroPontoDTOEntradaInserir {

    private Usuario usuario;
    private TipoRegistroEnum tipoRegistro;

    public RegistroPontoDTOEntradaInserir() { }

    public Usuario getUsuario() { return usuario; }

    public void setUsuario(Usuario usuario) { this.usuario = usuario; }

    public TipoRegistroEnum getTipoRegistro() { return tipoRegistro; }

    public void setTipoRegistro(TipoRegistroEnum tipoRegistro) { this.tipoRegistro = tipoRegistro; }

    public RegistroPonto converterRegistroPonto() {
        RegistroPonto registroPonto = new RegistroPonto();
        registroPonto.setUsuario(this.usuario);
        registroPonto.setTipoRegistro(this.tipoRegistro);

        return registroPonto;
    }

}
