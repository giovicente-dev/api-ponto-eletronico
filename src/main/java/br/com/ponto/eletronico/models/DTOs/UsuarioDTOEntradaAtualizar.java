package br.com.ponto.eletronico.models.DTOs;

import br.com.ponto.eletronico.models.Usuario;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import java.time.LocalDate;

public class UsuarioDTOEntradaAtualizar {

    private String nome;

    @CPF(message = "CPF inválido")
    private String cpf;

    @Email(message = "E-mail inválido")
    private String email;

    public UsuarioDTOEntradaAtualizar() { }

    public String getNome() { return nome; }

    public void setNome(String nome) { this.nome = nome; }

    public String getCpf() { return cpf; }

    public void setCpf(String cpf) { this.cpf = cpf; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public Usuario converterParaUsuario() {
        Usuario usuario = new Usuario();

        usuario.setNome(this.nome);
        usuario.setCpf(this.cpf);
        usuario.setEmail(this.email);

        return usuario;
    }

}
