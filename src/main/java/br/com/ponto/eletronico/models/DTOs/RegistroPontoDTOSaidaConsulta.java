package br.com.ponto.eletronico.models.DTOs;

import br.com.ponto.eletronico.models.RegistroPonto;

public class RegistroPontoDTOSaidaConsulta {

    int horasTrabalhadas;
    Iterable<RegistroPonto> registrosPonto;

    public RegistroPontoDTOSaidaConsulta() { }

    public int getHorasTrabalhadas() { return horasTrabalhadas; }

    public void setHorasTrabalhadas(int horasTrabalhadas) { this.horasTrabalhadas = horasTrabalhadas; }

    public Iterable<RegistroPonto> getRegistrosPonto() { return registrosPonto; }

    public void setRegistrosPonto(Iterable<RegistroPonto> registrosPonto) { this.registrosPonto = registrosPonto; }

}
