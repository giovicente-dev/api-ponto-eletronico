package br.com.ponto.eletronico.models.DTOs;

import br.com.ponto.eletronico.enums.TipoRegistroEnum;

import java.time.LocalDateTime;

public class RegistroPontoDTOSaidaInserir {

    private LocalDateTime dataHoraRegistro;
    private TipoRegistroEnum tipoRegistro;

    public RegistroPontoDTOSaidaInserir() { }

    public LocalDateTime getDataHoraRegistro() { return dataHoraRegistro; }

    public void setDataHoraRegistro(LocalDateTime dataHoraRegistro) { this.dataHoraRegistro = dataHoraRegistro; }

    public TipoRegistroEnum getTipoRegistro() { return tipoRegistro; }

    public void setTipoRegistro(TipoRegistroEnum tipoRegistro) { this.tipoRegistro = tipoRegistro; }

}
