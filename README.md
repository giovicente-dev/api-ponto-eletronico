Configurações do Projeto: Este projeto utiliza banco de dados H2, conforme configurações do application.properties.

===================================================================================

Endpoints + Exemplos:

**[POST] /usuarios - cria um usuário**

Body de entrada:
```json
{ 
    "nome": "Giovanni", 
    "cpf": "113.770.640-69", 
    "email": "giovicente@gmail.com", 
    "dataCadastro": "2020-07-04" 
}
````

Body de Saída:
```json
{ 
    "id": 1, 
    "nome": "Giovanni", 
    "cpf": "113.770.640-69", 
    "email": "giovicente@gmail.com", 
    "dataCadastro": "2020-07-05" 
}
````

**[GET] /usuarios - lista todos os usuarios**
Body de Saída:
```json
[ 
    { 
        "id": 1, 
        "nome": "Giovanni", 
        "cpf": "113.770.640-69", 
        "email": "giovicente@gmail.com", 
        "dataCadastro": "2020-07-05" 
        
    }, 
    { 
        "id": 2, 
        "nome": "Giovanni", 
        "cpf": "113.770.640-69", 
        "email": "giovicente@gmail.com", 
        "dataCadastro": "2020-07-05" 
        
    }, 
    { 
        "id": 3, 
        "nome": "Giovanni", 
        "cpf": "113.770.640-69", 
        "email": "giovicente@gmail.com", 
        "dataCadastro": "2020-07-05" 
        
    } 
]
````


**[GET] /usuarios/{id} - retorna um usuário por id**

Request Param: localhost:8080/usuarios/1

Body de Saída:
```json
{ 
    "id": 1, 
    "nome": "Giovanni", 
    "cpf": "113.770.640-69", 
    "email": "giovicente@gmail.com", 
    "dataCadastro": "2020-07-05" 
}
````

**[PUT] usuarios/{id} - atualiza usuário por id**

Request Param: localhost:8080/usuarios/1

Body de Entrada:
```json
{ 
    "nome": "Giovanni", 
    "cpf": "113.770.640-69", 
    "email": "gigio@gmail.com" 
}
````

Retorno: 204 (No Content)

**[POST] /registrosdeponto - inserir registro de ponto**

Body de Entrada:
```json
{ 
    "usuario":{ 
        "id": 1 
    }, 
    "tipoRegistro": "ENTRADA" 
}
````

Body de Saída:
```json
{ 
    "dataHoraRegistro": "2020-07-05T18:09:25.771", 
    "tipoRegistro": "ENTRADA" 
}
````

O tipoRegistro é um Enum, com os valores ENTRADA e SAIDA

**[GET] /registrosdeponto/{id_usuario}**

Request Param: localhost:8080/registrosdeponto/1

Body de Saída:
```json
{
    "horasTrabalhadas": 0, 
    "registrosPonto": [ 
        { 
            "id": 1, 
            "usuario": { 
                "id": 1, 
                "nome": "Giovanni", 
                "cpf": "113.770.640-69", 
                "email": "gigio@gmail.com", 
                "dataCadastro": "2020-07-05" 
            }, 
            "dataHoraRegistro": "2020-07-05T18:09:25.771", 
            "tipoRegistro": "ENTRADA" }, 
        { 
            "id": 2, 
            "usuario": { 
                "id": 1, 
                "nome": "Giovanni", 
                "cpf": "113.770.640-69", 
                "email": "gigio@gmail.com", 
                "dataCadastro": "2020-07-05" 
            }, 
            "dataHoraRegistro": "2020-07-05T18:10:15.147", 
            "tipoRegistro": "SAIDA" 
        } 
    ] 
}
````
